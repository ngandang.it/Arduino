
var ObjectId = require('mongodb').ObjectID;
var config = require(__dirname + '/../../config.json');
var _ = require('lodash');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('nodes');

var service = {};

service.get_by_id = get_by_id;
service.create = create;
service.update = update;
service.delete = _delete;
service.get_all = get_all;

module.exports = service;


function get_by_id(_id, fn) {
    var deferred = Q.defer();
    db.nodes.findById(
        _id,
        function (err, node) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            fn(node);
            deferred.resolve();
        }
    );

    return deferred.promise;
}

function create(node) {
    var deferred = Q.defer();
    node = _.omit(node, ["$$hashKey", "idx", "vehicle"]);
    for (var i = 0; i < node.relation.length; i++) {
        if(node.relation[i].speed == 0) {
            node.relation[i].speed = 400 * 1000; // m/s
        }
    }
    
    db.nodes.insert(
        node,
        function (err, doc) {
            if (err) {
                deferred.reject(err.name + ': ' + err.message);
            }
            deferred.resolve();
        }
    );
    return deferred.promise;
}

function get_all(callback) {
	db.nodes.find().toArray(function(e, d) {
		nodes = d;
        if (nodes == null) {
            console.log("MongoDB Error");
            nodes = [];
        }
        callback(nodes);
    });
}

function update(_id, node) {
    var deferred = Q.defer();
    
    db.nodes.findById(_id, function (err, node) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        update_node();
    });

    function update_node() {
        for (var i = 0; i < node.relation.length; i++) {
            if(node.relation[i].speed == 0) {
                node.relation[i].speed = 40 * 1000; // m/s
            }
        }
        
        var set = _.omit(node, ["_id", "$$hashKey", "idx", "vehicle"]);
        db.nodes.update(
        	{ _id: ObjectId(_id) }, 
			{ $set: set }
		);
    }

    return deferred.promise;
}

function _delete() {
	
}