var app = angular.module('node', []);
var wsUri = "ws://localhost:4567/";
var websocket = null;
var data = null;
var arr_data;
var stt = false;
var arr = [15, 15, 3];

var random = function (a, b) {
	return Math.floor(Math.random() * (b - a)) + a;
}

var connect = function($scope) {
	websocket = new WebSocket(wsUri);
	websocket.onopen = function(evt) {
		websocket.send(JSON.stringify({"_id": $scope.node._id}));
		$scope.btn_status = "Disconect";
		stt = true;
	};
	websocket.onmessage = function(evt) { 
		data = evt.data; 
		arr_data = data.split('#');
		arr[0] = parseInt(arr_data[0]);
		arr[1] = parseInt(arr_data[1]);
	};
	websocket.onerror = function(evt) { console.log(evt) };
	websocket.onclose = function(evt) {
		websocket = null;
		//stt = false;
		$scope.btn_status = "Connect";
	};
}

app.controller('main-controller', function($scope) {
	$scope.node = {};
	$scope.node._id = "";
	$scope.btn_status = "Connect";
	
	$scope.connect = function () {
		if ($scope.node._id == "") {
			alert("Please enter id");
			return;
		}

		if (!stt) {
			connect($scope);
		} else {
			websocket = null;
			stt = false;
			$scope.btn_status = "Connect";
		}
	}

	$scope.color = 0;
	

	angular.element(document).ready(function() {
		$scope.time = arr[0];

		setInterval(function() { 
			if (stt && !websocket) {
				connect($scope);
			}

			$scope.time--;

			if ($scope.time < 0){
				$scope.color = ($scope.color + 1) % 3;
				$scope.time = arr[$scope.color]; 
			}

			try {
				if (stt && websocket != null) {
					data = {time: $scope.time, color: $scope.color, idx: $scope.node.idx};

					websocket.send(JSON.stringify({time: $scope.time, color: $scope.color}));

					if ($scope.color == 0 && $scope.time == 0)
						websocket.send(JSON.stringify({vehicle: random(10, 50), color: $scope.color}));
				}
			}
			catch(ex){}
			

			$scope.$apply();
		}, 1000);

	});
});

window.onbeforeunload = function(e) {
	websocket.close();
 	return false;
};