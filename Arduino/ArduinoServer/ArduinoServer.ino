#include <SPI.h>
#include <Ethernet.h>
#include <SD.h>
#include <WebSocketClient.h>


WebSocketClient ws_client;
EthernetClient client, python_client;

char python_server_ip[] = "169.254.194.42";
int python_server_port = 10000;
char node_server_ip[] = "169.254.194.42";
int node_server_port = 4567;

float cycle_time = 20000.0;
bool s = true;
int second = 0, old = -1;
int color;
unsigned long times;



// ------------- Begin LED Enum -------------
enum LED {
    RED = 2,
    YELLOW = 3,
    GREEN = 5,

    LED_COLOR_LED = 0,
};
// ------------- End LED Enum -------------

// ------------- Begin LedTime Class -------------
class LedTime {
public:
    LedTime()
    {
        leds[0] = 10000;   //  r 
        leds[2] = 3000;  //  y
        leds[1] = 25000;   //  g
    }
    unsigned long get_time()
    {
        if (idx == 3)
            idx = 0;
        return leds[idx++];
    }

    int set_time(unsigned long r, unsigned long g) {
        leds[0] = r * 1000;
        leds[1] = g * 1000;
    }

private:
    unsigned long leds[3];
    int idx = 0;
};

// ------------- End LedTime Class -------------

// ------------- Begin TrafficLight Class -------------
class TrafficLight {
    LedTime* led_time;
    int color = 0;

    void init()
    {
        pinMode(LED::RED, OUTPUT);
        digitalWrite(LED::RED, LOW);
        pinMode(LED::YELLOW, OUTPUT);
        digitalWrite(LED::YELLOW, LOW);
        pinMode(LED::GREEN, OUTPUT);
        digitalWrite(LED::GREEN, LOW);
    }
    int change_led_color()
    {
        if (color == 0) {
            digitalWrite(LED::RED, HIGH);
            digitalWrite(LED::GREEN, LOW);
            digitalWrite(LED::YELLOW, LOW);
        } else if (color == 1) {
            digitalWrite(LED::GREEN, HIGH);
            digitalWrite(LED::RED, LOW);
            digitalWrite(LED::YELLOW, LOW);
        } else if (color == 2) {
            digitalWrite(LED::YELLOW, HIGH);
            digitalWrite(LED::GREEN, LOW);
            digitalWrite(LED::RED, LOW);
        }
        int ret = color;
        color = (color + 1) % 3;
        return ret;
    }

public:
    TrafficLight()
    {
        led_time = new LedTime();
        init();
    }
    void update(int current_time, int &col, unsigned long &times, bool &upd)
    {
        if (current_time <= 0) {
            col = change_led_color();
            times = led_time->get_time();
            upd = true;
        } else {
            ws_client.send("{\"time\":" + String(current_time) + ",\"color\":" + String(col) + "}");
            upd = false;
        }
    }

    void set_led_time(int r, int g) {
        led_time->set_time(r, g);
    }
};
// ------------- End TrafficLight Class -------------

// ------------- Begin Process -------------
TrafficLight tl;
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(169, 254, 194, 43);
EthernetServer server(80);
File webFile;

void setup()
{
    // disable Ethernet chip
    pinMode(10, OUTPUT);
    digitalWrite(10, HIGH);

    Serial.begin(9600);

    Serial.println("Initializing SD card...");
    if (!SD.begin(4)) {
        Serial.println("ERROR - SD card initialization failed!");
        return;
    }
    Serial.println("SUCCESS - SD card initialized.");

    Ethernet.begin(mac, ip);
    server.begin();

    do {
        if(ws_client.connect(node_server_ip, "/", node_server_port) == 1)
            break;
        delay(1000);
    } while (true);
    
    Serial.println("Node server was connected");

    do {
        if(python_client.connect(python_server_ip, python_server_port) == 1)
            break;
        delay(1000);
    } while (true);
    
    Serial.println("Python server was connected");

    
    ws_client.setDataArrivedDelegate(dataArrived);
    Serial.println("Set Data Arrived Delegate function");

    tl.update(0, color, times, s);
    cycle_time = times;
    webFile = SD.open("pic.jpg");


    
    ws_client.send("{\"_id\":\"58f33959b6c901214c7ae7a9\"}");
    Serial.println("Running .... .... .... ");
}

bool can_act = false;
unsigned long time;
void loop()
{
    time = millis();
    //    this open and send file to python
    if (!client.connected()) {
        client = server.available();
        delay(50);
    } else {
        if (webFile.available()) {
            char buf[128];
            int idx = 0;
            while(webFile.available()) {
                buf[idx++] = webFile.read();
                if (idx > 127) {
                    client.write(buf, 128);
                    idx = 0;
                    break;
                }
            }
            if (idx > 0)
                client.write(buf, idx);
        } else {
            client.stop();
            webFile.close();
            webFile = SD.open("pic.jpg");
            Serial.println("Send file");
        }
    }

    //    this get data from python and send it to node server 
    if(python_client.available()){
        int c = 0;
        while (python_client.available())
            c = c * 10 + python_client.read() - 48;
        ws_client.send("{\"vehicle\":" + String(c) + ",\"color\":" + String(color) + "}");
    }
    
    ws_client.monitor();

    cycle_time = cycle_time - (millis() - time);
    second = cycle_time / 1000;
    if (second != old) {
        if (can_act && second == 3) {
            python_client.print("1");
            can_act = false;
        }
        tl.update(second, color, times, s);
        if (s) {
            cycle_time = times;
            if (color == LED::LED_COLOR_LED) {
                can_act = true;
            }
        }
        old = second;
    }
}

void dataArrived(WebSocketClient client, String data) {
    char l = data.length(), g = 0, r = 0;
    bool t = false;
    for (byte i = 0; i < l; i++){ 
        if (data[i] == '#') {
            t = true;
            continue;
        }
        if (t) {
            g = g * 10 + data[i] - 48;
        } else {
            r = r * 10 + data[i] - 48;
        }
    }

    tl.set_led_time(r, g);
}
// ------------- End Process -------------
