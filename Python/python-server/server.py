import socket
import urllib
import cv2
import time

carc_path = "./car.xml"
car_cascade = cv2.CascadeClassifier(carc_path)



server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print "Starting server on http://169.254.183.47:10000"
server.bind((socket.gethostname(), 10000))
server.listen(1)

while True:
    conn, client = server.accept()
    print "Client ip:", client[0]
    image_path = str(time.time() * 100) + ".jpg"
    
    while True:
        data = conn.recv(1024)
        if data == "1":
            try:
                url = "http://" + client[0] + "/"
                print "get image from " + url

                urllib.urlretrieve(url, image_path)

                image = cv2.imread(image_path)
                gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                cars = car_cascade.detectMultiScale(gray, 1.1, 1)

                print "num ", len(cars)
                conn.send(str(len(cars)))

            except  Exception as ex:
                print "error: " + str(ex)
                conn.close()

            data = ""
