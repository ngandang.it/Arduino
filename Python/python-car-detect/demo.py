import cv2
import sys

# Get user supplied values
imagePath = "./demo.jpg"
cascPath = "./car.xml"

# Create the haar cascade
carCascade = cv2.CascadeClassifier(cascPath)

# Read the image
image = cv2.imread(imagePath)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Detect faces in the image
cars = carCascade.detectMultiScale(gray, 1.1, 1)

print("Found {0} cars!".format(len(cars)))

# Draw a rectangle around the faces
for (x, y, w, h) in cars:
    cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)

cv2.imshow("Cars found", image)
cv2.waitKey(0)
